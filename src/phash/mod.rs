pub mod dct;

/// return the hashs for some graysacle data
pub fn digest(data: &[u8]) -> u64 {
    assert_eq!(data.len(), 32*32);

    let mut dct = [0.0f32; 32 * 32];

    // compute dct
    dct::dct_II_2D(data, &mut dct);

    // filter out high frequencies
    // see : https://www.hackerfactor.com/blog/index.php?/archives/432-looks-like-it.html#c1410
    let mut sum = 0.0f32;
    let mut low_freq = [0.032; 64];
    for i in 0..8 {
        for j in 0..8 {
            low_freq[i * 8 + j] = dct[i * 32 + j];
            sum += dct[i * 32 + j];
        }
    }
    sum -= low_freq[0];

    // compute mean without lowest frequency component (the first one)
    let mean = sum / 63.0f32;

    // Binarize against the mean
    low_freq
        .iter()
        .fold(0u64, |k, v| (k << 1) | u64::from(*v >= mean))
}

#[cfg(test)]
mod tests {
    #[test]
    #[should_panic]
    fn wrong_dimension() {
        let oversized = vec![0; 2048];
        _ = super::digest(&oversized);
    }
}