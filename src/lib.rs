pub mod groups;
pub mod phash;
pub mod imgprocess; 

use std::path::PathBuf;
use std::sync::mpsc::{self, Receiver, Sender};
use std::sync::{Arc, Mutex};
use std::thread::{self, JoinHandle};

use anyhow::Result;
use clap::Parser;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use walkdir::{DirEntry, WalkDir};

use groups::SimilarityIndex;
use imgprocess::decode_and_preprocess;

/// Find similar pictures
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Root path to start looking at
    pub path: String,

    /// Do not show progress
    #[arg(long)]
    pub noprogress: bool,

    /// Do not rotate picture to follow EXIF orientation
    #[arg(long)]
    pub norotate: bool,

    /// Relax similarity threshold
    #[arg(short, long)]
    pub more: bool,

    /// Set the amount of concurrent workers
    #[arg(short, long)]
    pub workers: Option<usize>,
}

/// Determines if a fs entry is a jpeg file
/// Note: it only looks at the file extension
fn is_jpeg(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| {
            s.ends_with(".jpg")
                || s.ends_with(".jpeg")
                || s.ends_with(".JPG")
                || s.ends_with(".JPEG")
        })
        .unwrap_or(false)
}


/// Launch a thread looping over a stream (channel) of filepath to be processed
fn spawn_worker(
    input_chan: Arc<Mutex<Receiver<PathBuf>>>,
    output_chan: Sender<(u64, PathBuf)>,
    norotate: bool,
    number: usize,
    pb: Option<ProgressBar>,
) -> JoinHandle<()> {
    thread::spawn(move || {
        if let Some(pb) = &pb {
            pb.set_prefix(format!("[worker {}]", number));
        }
        loop {
            // Receive a filepath to process
            let message = {
                let lock = input_chan
                    .lock()
                    .expect("Worker thread unable to lock job receiver");
                lock.recv()
            };

            // An error here means all files have been processed
            let path = match message {
                Ok(path) => path,
                Err(..) => break,
            };

            // Eventually update the progress indicator
            if let Some(pb) = &pb {
                pb.set_message(format!("{}", path.display()));
                pb.inc(1);
            }

            // Heavy work is here
            let data = match decode_and_preprocess(path.as_path(), !norotate) {
                Ok(data) => data,
                Err(..) => continue,
            };

            // Get the hash
            let hash = phash::digest(&data);

            // Send back the hash and the path
            if let Err(_snderr) = output_chan.send((hash, path)) {
                break;
            }
        }
    })
}

pub fn run(args: &Args) -> Result<SimilarityIndex> {

    let mut index = SimilarityIndex::new(if args.more { 8 } else { 2 });

    let walker = WalkDir::new(&args.path)
        .into_iter()
        .filter_map(Result::ok)
        .filter(is_jpeg);

    // Setup parallel pipeline
    let (fanout_tx, fanout_rx) = mpsc::channel::<PathBuf>();
    let (fanin_tx, fanin_rx) = mpsc::channel::<(u64, PathBuf)>();
    let mutex_rx = Arc::new(Mutex::new(fanout_rx));

    let mb = MultiProgress::new();

    let num_worker = match args.workers {
        Some(num) => std::cmp::max(num, 1_usize),
        None => std::thread::available_parallelism()
            .map(usize::from)
            .unwrap_or(1_usize),
    };

    let workers: Vec<JoinHandle<()>> = (1..=num_worker)
        .map(|n| {
            let pb = match args.noprogress {
                false => Some(
                    mb.add(
                        ProgressBar::new_spinner().with_style(
                            ProgressStyle::with_template("{prefix:.bold.dim} {spinner} {wide_msg}")
                                .unwrap()
                                .tick_chars("⠋⠙⠹⠸⢰⣰⣠⣄⣆⡆⠇⠏"),
                        ),
                    ),
                ),
                true => None,
            };
            spawn_worker(
                mutex_rx.clone(),
                fanin_tx.clone(),
                args.norotate,
                n,
                pb,
            )
        })
        .collect();
    drop(fanin_tx); // Drop the sender for this thread as it's not needed

    // Fan out filepath to workers
    for entry in walker {
        let path = entry.into_path();

        if let Err(snderr) = fanout_tx.send(path) {
            eprintln!("could not process {}", snderr.0.display());
        }
    }
    drop(fanout_tx); // Dropping this will signal the worker that no more jobs will come

    // Start comparing hashes computed by workers
    while let Ok((hash, path)) = fanin_rx.recv() {
        index.update(hash, path);
    }

    // Wait for workers
    // Note: is it needed ?
    for worker in workers {
        worker.join().expect("worker panicked");
    }

    Ok(index)
}
