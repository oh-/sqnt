mod exif;

use std::path::Path;

use anyhow::Result;

/// Decodes a jpeg file to grayscale, eventually rotates the result and resizes it to 32x32px
pub fn decode_and_preprocess(path: &Path, rotate: bool) -> Result<Vec<u8>> {
    // TODO : Reuse buffers here instead allocating each time
    let raw = std::fs::read(path)?;
    let mut img = image::load_from_memory(&raw)?.to_luma8();

    if rotate {
        let exif = exif::parse(&raw)?;
        if let Some(value) = exif.orientation {
            /*  https://sirv.com/help/articles/rotate-photos-to-be-upright/
            https://exiftool.org/TagNames/EXIF.html */
            if value == 2 {
                img = image::imageops::flip_horizontal(&img);
            } else if value == 3 {
                img = image::imageops::rotate180(&img);
            } else if value == 4 {
                img = image::imageops::flip_vertical(&img);
            } else if value == 5 {
                img = image::imageops::flip_horizontal(&img);
                img = image::imageops::rotate270(&img);
            } else if value == 6 {
                img = image::imageops::rotate90(&img);
            } else if value == 7 {
                img = image::imageops::flip_horizontal(&img);
                img = image::imageops::rotate90(&img);
            } else if value == 8 {
                img = image::imageops::rotate270(&img);
            }
        }
    };

    Ok(image::imageops::resize(
        &img,
        32,
        32,
        image::imageops::Triangle,
    ).into_raw())
}