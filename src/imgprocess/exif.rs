/// Extremely stripped down EXIF parser that only get the image orientation
use anyhow::Context;

const APP1: [u8; 2] = [0xff, 0xe1];
const SOS: [u8; 2] = [0xff, 0xda];
const EXIF_ID: [u8; 6] = [0x45, 0x78, 0x69, 0x66, 0x00, 0x00];

enum Endianess {
    Big,
    Litle,
}

pub struct EXIFView {
    pub orientation: Option<u16>,
}

fn read_u16(data: &[u8], end: &Endianess) -> Result<u16, anyhow::Error> {
    let chunk = data[0..2]
        .try_into()
        .with_context(|| "could not parse APP bloc size")?;
    match end {
        Endianess::Litle => Ok(u16::from_le_bytes(chunk)),
        Endianess::Big => Ok(u16::from_be_bytes(chunk)),
    }
}

fn read_u32(data: &[u8], end: &Endianess) -> Result<u32, anyhow::Error> {
    let chunk = data[0..4]
        .try_into()
        .with_context(|| "could not parse APP bloc size")?;
    match end {
        Endianess::Litle => Ok(u32::from_le_bytes(chunk)),
        Endianess::Big => Ok(u32::from_be_bytes(chunk)),
    }
}

fn find_app_marker(data: &[u8]) -> Option<usize> {
    let start = data
        .windows(2)
        .position(|window| window == APP1 || window == SOS)?;

    if data[start..start + 2] == SOS {
        return None;
    }

    Some(start)
}

pub fn parse(data: &[u8]) -> Result<EXIFView, anyhow::Error> {
    let exif_raw = scan_exif_chunk(data)?;
    match exif_raw {
        Some(buffer) => parse_exif_data(buffer),
        None => Ok(EXIFView { orientation: None }),
    }
}

pub fn scan_exif_chunk(data: &[u8]) -> Result<Option<&[u8]>, anyhow::Error> {
    let mut cursor = 0;
    if let Some(start) = find_app_marker(&data[cursor..]) {
        cursor += start;
        let bl_size = data[cursor + 2..cursor + 4]
            .try_into()
            .with_context(|| "could not parse APP bloc size")?;
        let size = u16::from_be_bytes(bl_size) as usize;
        if data[cursor + 4..cursor + 10] == EXIF_ID {
            return Ok(Some(&data[cursor + 10..cursor + size]));
        }
    }
    Ok(None)
}

pub fn parse_exif_data(data: &[u8]) -> Result<EXIFView, anyhow::Error> {
    let mut ret = EXIFView { orientation: None };

    let mut idx = 0;
    let endianess = match data[idx..2] {
        [0x49, 0x49] => Endianess::Litle,
        [0x4D, 0x4D] => Endianess::Big,
        _ => return Err(anyhow::anyhow!("unexpected EXIF format")),
    };

    idx += 2;
    let fortytwo = read_u16(&data[idx..], &endianess)?;
    if fortytwo != 42 {
        return Err(anyhow::anyhow!("unexpected EXIF format"));
    };

    idx += 2;
    idx = read_u32(&data[idx..], &endianess)? as usize; // jump to first ifd

    let ndirentry = read_u16(&data[idx..], &endianess)? as usize;

    idx += 2;
    for ide in 0..ndirentry {
        let idx = idx + ide * 12;
        let tag = read_u16(&data[idx..], &endianess)?;
        let _size = read_u32(&data[idx + 4..], &endianess)? as usize;
        let _offset = read_u32(&data[idx + 8..], &endianess)? as usize;
        if tag == 0x112 {
            // Orientation
            ret.orientation = Some(
                read_u16(&data[idx + 8..], &endianess)
                    .with_context(|| "Something wrong with the EXIF orientaion")?,
            );
            return Ok(ret);
        }
    }

    Ok(ret)
}
