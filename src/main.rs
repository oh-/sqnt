use anyhow::Result;
use clap::Parser;
use sqnt::Args;

/// This is the entry point of the command line binary
fn main() -> Result<()> {
    let args = Args::parse();

    let index = sqnt::run(&args)?;
    
    index.summary();

    Ok(())
}
