use std::path::{PathBuf, Path};

pub type Group = Vec<usize>;

/// Data structure to allow going from an image to other similar ones
pub struct Record {
    path: PathBuf,
    group: Option<usize>, // Here, group is an offset to be found in SimilarityIndex.groups
}

/// Data structure to group similar images
/// Notes:
/// Each analysed images results in an entry in both vectors .hashes and .records at the same offset
/// Design choice inspired from https://en.wikipedia.org/wiki/AoS_and_SoA#Structure_of_arrays
pub struct SimilarityIndex {
    hashes: Vec<u64>, // Memory contiguity for faster hashes lookup
    records: Vec<Record>,
    groups: Vec<Group>, // A group is a bunch of offsets refering to SimilarityIndex.records and SimilarityIndex.hashes
    threshold: u8,
}

impl SimilarityIndex {
    /// Return an empty structure ready to be used
    pub fn new(threshold: u8) -> SimilarityIndex {
        SimilarityIndex {
            hashes: vec![],
            records: vec![],
            groups: vec![],
            threshold,
        }
    }

    /// Adds an image to the index
    /// Current algorithm is time complexity is O(N) where N is the number of hashes already added
    pub fn update(&mut self, hash: u64, path: PathBuf) {
        // Prepare a record to be inserted
        let idx = self.hashes.len();
        let mut rec = Record { path, group: None };

        // Scan hashes, get first similar
        let similar = self
            .hashes
            .iter()
            .enumerate()
            .find(|(_, &h)| (hash ^ h).count_ones() <= self.threshold as u32)
            .map(|(n, _)| n);

        // If something was found
        if let Some(tidx) = similar {
            match self.records[tidx].group {
                None => {
                    // Create a new group with both records
                    let newgroup = vec![tidx, idx];
                    self.groups.push(newgroup);

                    // Update records with new group index
                    let gid = self.groups.len() - 1;
                    self.records[tidx].group = Some(gid);
                    rec.group = Some(gid);
                }
                Some(group) => {
                    // Add record to an exisitng group
                    rec.group = Some(group);
                    self.groups[group].push(idx);
                }
            }
        }

        // Insert new record
        self.hashes.push(hash);
        self.records.push(rec);
    }

    /// Prints the groups
    /// TODO: replace this with a Display trait
    pub fn summary(&self) {
        for g in &self.groups {
            for r in g {
                print!("{} ", self.records[*r].path.display());
            }
            println!();
        }
    }

    /// Retuns the number of groups aka the number of cluster of images that might be similar
    pub fn len(&self) -> usize {
        self.groups.len()
    }

    /// Returns a collections of path of similar image 
    /// TODO: build a better UX for this, maybe maybe iterator
    pub fn get_group(&self, offset: usize) -> Option<Vec<&Path>> {
        self.groups.get(offset)
            .map(|v| v.iter())
            .map(|it| it.map(|idx| self.records[*idx].path.as_path()))
            .map(|it| it.collect::<Vec<&Path>>())
    }
}


#[cfg(test)]
mod tests {
    use std::{path::PathBuf, str::FromStr};

    use super::SimilarityIndex;

    #[test]
    fn insertion() {
        let mut si = SimilarityIndex::new(0);
        si.update(0, PathBuf::from_str("first").unwrap());
        si.update(1, PathBuf::from_str("second").unwrap());
        si.update(0, PathBuf::from_str("third").unwrap());

        assert_eq!(si.records.len(), si.hashes.len());
        assert_eq!(si.records.len(), 3);
        assert_eq!(si.get_group(0).unwrap()[0], PathBuf::from_str("first").unwrap().as_path());

    }

    #[test]
    fn grouping() {
        let mut si = SimilarityIndex::new(0);
        si.update(0, PathBuf::from_str("first").unwrap());
        si.update(1, PathBuf::from_str("second").unwrap());
        si.update(0, PathBuf::from_str("third").unwrap());

        assert_eq!(si.len(), 1);
    }
}