
# sqnt
![Project unmaintained](https://img.shields.io/badge/project-unmaintained-red.svg)


A tool to find similar pictures.



## Features

- Finds similar pictures
- Respects EXIF orientation tag
- Is not slow (work in progress to make it really fast)
- Outputs a fair amount of false positive


## Alternatives

An easier to use, better maintained, with a gui solution is [czkawka](https://github.com/qarmin/czkawka)

## How to use

Get the last release : [HERE](https://gitlab.com/oh-/sqnt/-/releases)

Then run :

```bash
chmod +x sqnt-x86_64-linux-gnu

./sqnt-x86_64-linux-gnu /path/to/your/pictures/
```

The output looks like that:

```
file1 file2
file3 file4 file5
```

Where files on the same line are deemed similar. It's up to you to decide what to do next.

## Build from source

Clone the project

```bash
  git clone https://gitlab.com/oh-/sqnt
```

Go to the project directory

```bash
  cd sqnt
```

Run with cargo

```bash
  cargo run -r -- /path/to/your/pictures/
```


## Roadmap

- Measure everything and address any slowness
- Reduce dependencies
- Maybe try other algorithm than phash


## License

```
 Copyright 2023 Olivier Hoarau

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```

