# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

### Changed

-

## [2023-07-02 - 1.3.0]

### Improved

- Speed (30x on DCT transform)

## [2023-06-24 - 1.2.0]

### Added

- An option to control the amount of concurrent workers

### Changed

- The option `-m` relax the acceptable hamming distance to 8 bits (formerly 5)
- Some code refactor

### Fixed

- Fix the option to turn off progress

## [2023-06-22 - 1.1.0]

### Added

- Readme and license 
- A progress indicator

### Changed

- Use multi-threading
- Use another DCT Transform implementation

### Fixed

- Properly compute pHash (skip lowest frequency)

## [2023-06-08 - 1.0.0]

### Added

- Some code to match close enough hashes
- A perceptual hash implementation
- Some code to decode and resize/roate jpegs
- An exif parser
- A changelog