use sqnt::{Args, run};

/// This will test against the same image rotated and flipped in every direction
/// The images have their EXIF Orientation tag set so they should all be displayed in the same direction
/// and thus they should be considered similar
#[test]
pub fn corpus_orientation() {
    let mut args = Args {
        path: String::from("tests/corpus/orientation"),
        noprogress: true,
        norotate:false,
        more:false,
        workers: Some(4),
    };
    let idx = run(&args);
    assert!(idx.is_ok(), "it should run through this corpus ok");

    let idx = idx.unwrap();
    assert_eq!(idx.len(), 1, "there should be only one group");
    assert_eq!(idx.get_group(0).unwrap().len(), 8, "this group should have 8 files");

    // Now run it again without rotation
    args.norotate = true;

    let idx = run(&args);
    assert!(idx.is_ok(), "it should run through this corpus ok");

    let idx = idx.unwrap();
    assert_eq!(idx.len(), 0, "it should not find any groups without rotation");
}

/// This will test against the same image encoded with different chroma subsampling settings
/// It serves as jpeg decoder validation (I've had surprises)
#[test]
pub fn corpus_encoding() {
    let args = Args {
        path: String::from("tests/corpus/encoding"),
        noprogress: true,
        norotate:false,
        more:false,
        workers: Some(4),
    };
    let idx = run(&args);
    assert!(idx.is_ok(), "it should run through this corpus ok");

    let idx = idx.unwrap();
    assert_eq!(idx.len(), 1, "there should be only one group");
    assert_eq!(idx.get_group(0).unwrap().len(), 4, "this group should have 4 files");

}