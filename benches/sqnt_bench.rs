use std::path::Path;

use criterion::{criterion_group, criterion_main, Criterion};
use sqnt::phash;

pub fn benchmark_dct_custom(c: &mut Criterion) {

    let input = vec![1; 1024];
    let mut output = vec![0.0f32; 1024];

    c.bench_function("DCT 32x32", |b| b.iter(|| phash::dct::dct_II_2D(&input, &mut output)));
}

pub fn benchmark_decodeproc(c: &mut Criterion) {
    use sqnt::imgprocess::decode_and_preprocess;

    c.bench_function("Decode and process", |b| b.iter(|| decode_and_preprocess(Path::new("tests/corpus/rotation/house.jpeg"), false)));
}

criterion_group!(benches, benchmark_dct_custom, benchmark_decodeproc);
criterion_main!(benches);